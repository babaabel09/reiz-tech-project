import { ChangeEvent } from "react";
import { FaSearch } from "react-icons/fa";

interface NavbarProps {
  handleSortAlphabetically: (event: ChangeEvent<HTMLSelectElement>) => void;
  handleSortByRegion: (event: ChangeEvent<HTMLSelectElement>) => void;
  handelSearch: (event: ChangeEvent<HTMLInputElement>) => void;
  handleReset: () => void;
  input: string;
  selectedValue: string;
  selectedValueRegion: string;
}

const Navbar: React.FC<NavbarProps> = ({
  handleSortAlphabetically,
  handleSortByRegion,
  handleReset,
  handelSearch,
  selectedValue,
  selectedValueRegion,
  input,
}) => {
  // const { handleDescendSort } = Result();
  // const handleDescendSort = () => {};

  return (
    <div className="sticky top-0 bg-blue-50 py-2 shadow-[0_3px_0px_0px_rgba(0,0,0,0.1)]">
      <div className="sm:w-[400px] w-[280px] bg-white rounded-xl h-[2.5rem]  px-2 shadow-[0px_0px_8px_#ddd] flex items-center">
        <FaSearch className="text-[royalblue]" />
        <input
          className="border-0 focus:outline-none w-full text-xl ms-1"
          placeholder="Type to search a country..."
          value={input}
          onChange={handelSearch}
        />
      </div>
      <div className="sm:flex flex-row-reverse justify-between items-center mt-3 sm:space-y-0 space-y-5 ">
        <select value={selectedValueRegion} onChange={handleSortByRegion}>
          <option value="">Sort by:</option>
          <option value="area">Countries smaller than Lithuania</option>
          <option value="region">Countries in Oceania region</option>
        </select>
        <div className="  ">
          <button className="w-[120px] sm:my-0" onClick={handleReset}>
            All Countries
          </button>
          <select
            className="ms-4 w-[120px] "
            value={selectedValue}
            onChange={handleSortAlphabetically}
          >
            <option value="">Sort by:</option>
            <option value="Ascending">Ascending</option>
            <option value="Descending">Descending</option>
          </select>
        </div>
      </div>
    </div>
  );
};
export default Navbar;
