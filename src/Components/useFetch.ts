import { useEffect, useState } from "react";

interface Country {
  name: string;
  region: string;
  area: number;
}

export const useFetch = (url: string) => {
  const [data, setData] = useState<Country[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [dataFilters, setDataFilters] = useState<Country[]>([]);

  useEffect(() => {
    setLoading(true);
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        setData(res);
        setDataFilters(res);
      })
      .catch((err) => setError(err.message || "An error occurred"))
      .finally(() => setLoading(false));
  }, [url]);

  const sortDataAscend = (arr: Country[]) => {
    arr.sort((a, b) => a.name.localeCompare(b.name));
  };
  sortDataAscend(data);

  return { data, loading, error, setData, dataFilters };
};
