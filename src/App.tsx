import { useState, ChangeEvent, useEffect } from "react";
import Navbar from "./Components/Navbar";
import ReactPaginate from "react-paginate";
import { useFetch } from "./Components/useFetch";

type pageNo = {
  selected: number;
};
interface Country {
  name: string;
  region: string;
  area: number;
}
function App() {
  const [display, setdisplay] = useState<React.ReactNode[]>([]);
  const [pageData, setPageData] = useState(0);
  const [selectedValue, setSelectedValue] = useState("");
  const [selectedValueRegion, setSelectedValueRegion] = useState("");
  const [input, setInput] = useState("");

  const { data, loading, error, setData, dataFilters } = useFetch(
    `https://restcountries.com/v2/all?fields=name,region,area`
  );

  const handelSearch = (event: ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value.toLowerCase();
    setInput(inputValue);

    const searchData = dataFilters.filter((item) =>
      item.name.toLowerCase().includes(inputValue)
    );

    setData(searchData);
  };

  const handleReset = () => {
    setPageData(0);
    dataFilters.sort((a, b) => a.name.localeCompare(b.name));
    setData(dataFilters);
    setSelectedValue("");
    setSelectedValueRegion("");
    scrollToTop();
  };

  const handleSortAlphabetically = (e: ChangeEvent<HTMLSelectElement>) => {
    setPageData(0);
    setSelectedValue(e.target.value);
    scrollToTop();
  };

  const sortDataAscend = (arr: Country[]) => {
    arr.sort((a, b) => a.name.localeCompare(b.name));
  };
  const sortDataDescend = (arr: Country[]) => {
    arr.sort((a, b) => b.name.localeCompare(a.name));
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const handleSortByRegion = (e: ChangeEvent<HTMLSelectElement>) => {
    setPageData(0);
    setSelectedValueRegion(e.target.value);

    switch (e.target.value) {
      case "area": {
        const lithuaniaArr = dataFilters.filter((x) => x.name === "Lithuania");
        const lithuaniaArea = lithuaniaArr[0].area;
        const areaBelow = dataFilters.filter((y) => y.area! < lithuaniaArea);
        setData(areaBelow);
        break;
      }
      case "region": {
        const oceaniaCountries = dataFilters.filter(
          (country) => country.region === "Oceania"
        );
        setData(oceaniaCountries);
        break;
      }
      default:
        break;
    }
    scrollToTop();
  };

  useEffect(() => {
    const splitArrayIntoSets = (array: Country[], setSize: number) => {
      const sets = [];
      const totalSets = Math.ceil(array.length / setSize);

      for (let i = 0; i < totalSets; i++) {
        const startIndex = i * setSize;
        const endIndex = startIndex + setSize;
        const set = array.slice(startIndex, endIndex);
        sets.push(set);
      }

      return sets[pageData];
    };
    if (selectedValue === "Ascending") {
      sortDataAscend(data);
    } else if (selectedValue === "Descending") {
      sortDataDescend(data);
    }
    let displayedArr = splitArrayIntoSets(data, 10);

    let displayData = displayedArr?.map((x, ind) => {
      const { name, region, area } = x;
      return (
        <div
          className=" border-solid border-black rounded-2xl my-3 p-2 bg-blue-100"
          key={ind}
        >
          <h3 className="font-bold text-xl">Country: {name}</h3>
          <p>region: {region}</p>
          <p className="text-blue-700 font-bold">area: {area}</p>
        </div>
      );
    });
    setdisplay(displayData);
  }, [data, pageData, selectedValue]);

  const handlePageClick = (pageNo: pageNo) => {
    setPageData(pageNo.selected);

    const isLastPage = pageNo.selected === Math.ceil(data.length / 10) - 1;

    if (isLastPage) {
      setTimeout(() => {
        window.scrollTo({
          top: 0,
          behavior: "smooth",
        });
      }, 100);
    } else {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
  };

  return (
    <div className="App sm:px-14 pr-5 pl-3 pt-1 pb-5 bg-blue-50  ">
      <Navbar
        handleSortAlphabetically={handleSortAlphabetically}
        handleReset={handleReset}
        handleSortByRegion={handleSortByRegion}
        selectedValue={selectedValue}
        selectedValueRegion={selectedValueRegion}
        handelSearch={handelSearch}
        input={input}
      />
      <>
        {loading && <p>LOADING...</p>}
        {error && <p className="text-red-700">{error}</p>}

        {display}
      </>
      <ReactPaginate
        forcePage={pageData}
        breakLabel="..."
        nextLabel=">>"
        onPageChange={handlePageClick}
        marginPagesDisplayed={3}
        pageRangeDisplayed={3}
        pageCount={Math.ceil(data.length / 10)}
        previousLabel="<<"
        renderOnZeroPageCount={null}
        containerClassName={"pagination justify-center"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        previousLinkClassName={"page-link"}
        nextClassName={"page-item"}
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />
    </div>
  );
}

export default App;
